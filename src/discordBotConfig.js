const { ENVS } = require('./constants')
if (process.env.NODE_ENV !== ENVS.production) {
    require('dotenv').config()
}

module.exports = {
    token: process.env.BOT_TOKEN,
    mode: {
        production: process.env.NODE_ENV === ENVS.production,
        development: process.env.NODE_ENV === ENVS.development,
    },
    roles: {
        trainee: '850393978926006294',
    },
    channels: {
        log: '932593903369068554',
        question: '824364155318042634',
    },
    guild: {
        id: '821695508925644852',
        name: 'Passion for coding',
    },
}
