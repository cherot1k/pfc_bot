const { Client } = require('discord.js')
const config = require('./discordBotConfig.js')
const { INTENTS } = require('./constants')
const badWords = require('./modules/bad-words/index')
const voteSystem = require('./modules/vote-system/index')
const autoroleAndNicknameValidation = require('./modules/autorole&nickname-validation/index')
const questions = require('./modules/questions/index')
const antispamSystem = require('./modules/antispam-system/index')

const bot = new Client({
    intents: INTENTS,
})

badWords(bot)
autoroleAndNicknameValidation(bot)
antispamSystem(bot)

bot.on('ready', () => {
    console.log(`Bot ${bot.user.tag} has started!`)
    questions(bot).catch((e) => console.log(e))
    voteSystem(bot)
})

async function start() {
    await bot.login(config.token)
}

start().catch(console.error)
