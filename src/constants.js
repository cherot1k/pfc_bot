const INTENTS = 771
const ENVS = { production: 'production', development: 'development' }

module.exports = { INTENTS, ENVS }
