const TIME = 30000 // 30s
const COUNT_MESSAGES = 5
const MUTE_TIME = 10000 // 10s

module.exports = {
    TIME,
    COUNT_MESSAGES,
    MUTE_TIME,
}
