function safeInc(object, property) {
    object[property] = (object[property] || 0) + 1
}

function collectionToArray(collection) {
    return [...collection.values()]
}

function fetchLastAuthorMessages(channel, authorId) {
    return channel.messages
        .fetch({ limit: 50 })
        .then((messages) =>
            messages.filter((msg) => msg.author.id === authorId)
        )
}

module.exports = {
    safeInc,
    collectionToArray,
    fetchLastAuthorMessages,
}
