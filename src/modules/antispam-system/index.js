const { TIME, COUNT_MESSAGES, MUTE_TIME } = require('./constants')
const {
    safeInc,
    collectionToArray,
    fetchLastAuthorMessages,
} = require('./helpers')

let countMessages = {}

setInterval(() => {
    countMessages = {}
}, TIME)

module.exports = (bot) => {
    try {
        bot.on('messageCreate', (message) => {
            const authorId = message.author.id

            safeInc(countMessages, authorId)

            if (countMessages[authorId] === COUNT_MESSAGES) {
                try {
                    message.member.timeout(MUTE_TIME, 'Antispam System')
                } catch (e) {
                    console.log('failed to mute member', message.member.id)
                }

                try {
                    fetchLastAuthorMessages(message.channel, authorId).then(
                        (fetchedMessages) => {
                            const messages = collectionToArray(fetchedMessages)

                            message.channel.bulkDelete(
                                messages.slice(0, COUNT_MESSAGES)
                            )
                        }
                    )
                } catch (e) {
                    console.log(
                        'failed to delete author messages',
                        message.member.id
                    )
                }
            }
        })
    } catch (e) {
        console.log('init failed', e)
    }
}
