const config = require('../../discordBotConfig')
const { verifyNickname } = require('./regexp')

module.exports = (bot) => {
    bot.on('guildMemberAdd', (member) => {
        try {
            verifyNickname(member)
            member.roles.add(config.roles.trainee, 'autorole')
        } catch (e) {
            console.log(e)
        }
    })

    bot.on('guildMemberUpdate', (_, member) => {
        try {
            verifyNickname(member)
        } catch (e) {
            console.log(e)
        }
    })
}
