const { MessageEmbed } = require('discord.js')
const {
    guild: { id: guildId },
} = require('../../discordBotConfig')

/**
 * @typedef {{
 * 	label: string,
 * 	voted: string[]
 * }} ButtonData
 *
 * @typedef {{
 * 	name: string,
 * 	author: {
 * 		text: string,
 * 		iconURL: string
 * 	},
 * 	options: ButtonData[],
 * 	anonymous: boolean,
 * }} VoteData
 * */

/** @type {{[key: string]: VoteData}} */
const VOTES_STORE = {}

/**
 * Generates integer random id
 *
 * @returns {string} String like integer
 */
function generateRandomId() {
    // 27iq
    return Math.floor(Math.random() * 1e16).toString()
}

/**
 * Changes user vote. If user already voted for this removes vote
 *
 * @param {string} insertedId
 * @param {number} buttonId
 * @param {string} userId
 */
function changeVote(insertedId, buttonId, userId) {
    for (let idx = 0; idx < VOTES_STORE[insertedId].options.length; idx++) {
        const el = VOTES_STORE[insertedId].options[idx]

        let idxOf = el.voted.indexOf(userId)
        if (idxOf != -1) {
            el.voted.splice(idxOf, 1)
        } else if (buttonId == idx) {
            el.voted.push(userId)
        }
    }
}

/**
 * Makes one embed for creating or updating vote message
 *
 * @param {VoteData} voteData
 * @returns {import('discord.js').MessageEmbed}
 */
function makeEmbed(voteData) {
    const PROGRESS_CHAR = '▱'
    const PROGRESS_CHAR_FILLED = '▰'
    const $ = String.prototype.repeat.bind(PROGRESS_CHAR)
    const $$ = String.prototype.repeat.bind(PROGRESS_CHAR_FILLED)

    let builder = new MessageEmbed()
        .setTitle('Голосование' + (!voteData.anonymous ? ' (не анонимно)' : ''))
        .setDescription(voteData.name)
        .setFooter(voteData.author)

    let totalVotes = voteData.options.reduce((p, c) => p + c.voted.length, 0)
    if (totalVotes === 0) return builder // yes...

    /** @type {string[]} */
    let votedUsers = []

    builder.addFields(
        ...voteData.options.map((el) => {
            votedUsers.push(...el.voted)
            return {
                name: `${el.label} (${el.voted.length}/${totalVotes})`,
                value: `${$$(el.voted.length)}${$(
                    totalVotes - el.voted.length
                )} ${Math.round((el.voted.length * 100) / totalVotes)}%`,
                inline: false, // Check pull #10
            }
        })
    )

    if (!voteData.anonymous)
        builder.addField(
            'Проголосовали',
            votedUsers.map((e) => `<@${e}>`).join(', ')
        )

    return builder
}

/**
 * Makes components for message
 *
 * @param {number} insertId
 * @returns {import('discord.js').MessageActionRow}
 */
function makeComponents(insertId) {
    let arr = VOTES_STORE[insertId].options

    return [
        {
            type: 1,
            components: arr.map((el, idx) => ({
                type: 2,
                label: el.label,
                style: 2,
                customId: `vtsys-${insertId}-${idx}`,
            })),
        },
    ]
}

/**
 * @param {import('discord.js').BaseCommandInteraction<import('discord.js').CacheType>} iter
 */
async function onCommandCalled(iter) {
    /** @type {ButtonData[]} */
    let arr = []
    /** @type {string[]} */
    let options = iter.options.get('options').value?.split('/')
    /** @type {boolean} */
    let anonymous = iter.options.get('anonymous')?.value ?? true

    let insertId = generateRandomId()

    if (options.length < 2 || options.length > 5) {
        return await iter.reply({
            content: 'Укажите 2-5 варианта ответа',
            ephemeral: true,
        })
    }

    options.forEach((label) => arr.push({ label, voted: [] }))
    VOTES_STORE[insertId] = {
        name: iter.options.get('title').value,
        author: {
            text: iter.user.username,
            iconURL: iter.user.avatarURL(),
        },
        options: arr,
        anonymous,
    }

    await iter.reply({
        embeds: [makeEmbed(VOTES_STORE[insertId])],
        components: makeComponents(insertId),
    })
}

/**
 * @param {import('discord.js').ButtonInteraction<import('discord.js').CacheType>} iter
 */
async function onButtonClicked(iter) {
    /** @type {[undefined, string, string]} */
    // eslint-disable-next-line no-unused-vars
    let [_, insertedId, buttonId] = /^vtsys-(\d+)-(\d+)$/.exec(iter.customId)

    if (!VOTES_STORE[insertedId]) {
        return await iter.reply({
            content:
                'Невозможно проголосовать, так как информация о голосовании утрачена',
            ephemeral: true,
        })
    }

    changeVote(insertedId, buttonId, iter.user.id)

    await iter.update({
        embeds: [makeEmbed(VOTES_STORE[insertedId])],
        components: makeComponents(insertedId),
    })
}

async function init(/** @type {import("discord.js").Client<true>} */ bot) {
    bot.on('interactionCreate', async (iter) => {
        if (iter.isApplicationCommand() && iter.commandName === 'vote') {
            await onCommandCalled(iter)
        } else if (iter.isButton() && iter.customId.startsWith('vtsys-')) {
            await onButtonClicked(iter)
        }
    })

    await bot.application.commands.create(
        {
            name: 'vote',
            description: 'Начать голосование',
            options: [
                {
                    name: 'title',
                    type: 3,
                    required: true,
                    description: 'Наименование голосования',
                },
                {
                    name: 'options',
                    type: 3,
                    required: true,
                    description: 'Варианты ответов, разделённые слешом (/)',
                },
                {
                    name: 'anonymous',
                    type: 'BOOLEAN',
                    required: false,
                    description: 'Анонимное голосование (по умолчанию: да)',
                },
            ],
        },
        guildId
    )
}

module.exports = async (/** @type {import("discord.js").Client} */ bot) => {
    try {
        await init(bot)
    } catch (e) {
        console.error('Module vote-system init failed: ' + e)
    }
}
