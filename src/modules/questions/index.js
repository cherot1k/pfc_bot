const { createThread, deleteMessage, sendEmbededMessage } = require('./helpers')
const {
    channels: { question: channelId },
    guild: { id: guildId },
} = require('../../discordBotConfig')

async function init(bot) {
    try {
        bot.on('interactionCreate', async (interaction) => {
            try {
                if (interaction.commandName === 'google') {
                    const channelId = interaction.channelId
                    const request = interaction.options
                        .getString('title')
                        .split(' ')
                        .join('+')
                    const channel = await bot.channels.cache.get(channelId)
                    channel.send(
                        `Твой вопрос можно загуглить, вот тебе подсказка: https://www.google.com/search?q=${request}`
                    )
                    await interaction.reply({
                        content: 'Красавчик, пусть учатся гуглить!',
                        ephemeral: true,
                    })
                }
                if (interaction.commandName !== 'question') return // nice check (TODO: normal command manager)
                const interactionTitle = interaction.options.getString('title')
                const channel = await bot.channels.cache.get(channelId)
                await createThread(channel, interactionTitle, interaction.user)
                await interaction.reply({
                    content: 'Тред создан',
                    ephemeral: true,
                })
            } catch (e) {
                console.log(e)
            }
        })

        await bot.application.commands.create(
            {
                name: 'question',
                description: 'Задать вопрос',
                options: [
                    {
                        name: 'title',
                        type: 3,
                        required: true,
                        description: 'Запишите свой вопрос здесь',
                    },
                ],
            },
            guildId
        )

        await bot.application.commands.create(
            {
                name: 'google',
                description: 'Давай я погуглю за тебя',
                options: [
                    {
                        name: 'title',
                        type: 3,
                        required: true,
                        description: 'Запишите свой запрос здесь',
                    },
                ],
            },
            guildId
        )

        bot.on('messageCreate', (message) => {
            try {
                if (channelId === message.channelId && !message.author.bot) {
                    const messageId = message.id
                    deleteMessage(bot, messageId)
                    sendEmbededMessage(bot)
                }
            } catch (e) {
                console.log(e)
            }
        })
    } catch (e) {
        console.log(e)
    }
}

module.exports = async (bot) => {
    await init(bot)
}
