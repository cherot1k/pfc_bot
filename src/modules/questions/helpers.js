const { DAY_IN_MINUTES } = require('./constants')
const {
    channels: { question },
} = require('../../discordBotConfig')

const embedMessage = {
    type: 'rich',
    title: 'Как задать вопрос?',
    description:
        'Вопрос можно задать только через команду /question (здесь заголовок вопроса), после этого бот создаст тред с заголовком, который Вы ввели, и обсуждение будет вестись уже в треде.',
    color: 0x00ffff,
    image: {
        url: 'https://cdn.discordapp.com/attachments/932640289183645768/935155288363925594/unknown.png',
        height: 0,
        width: 0,
    },
}

async function createThread(channel, interactionTitle, user) {
    const createdThread = await channel.threads.create({
        name: interactionTitle,
        autoArchiveDuration: DAY_IN_MINUTES,
    })
    await createdThread.send(`<@${user.id}>, Ваш вопрос успешно создан.`)
}

async function sendEmbededMessage(bot) {
    const channel = await bot.channels.cache.get(question)

    channel.send({ embeds: [embedMessage] })
}

async function deleteMessage(bot, lastMessageId) {
    const channel = await bot.channels.cache.get(question)
    const message = channel.messages.cache.get(lastMessageId)
    if (message.author.bot) return
    const messages = (await channel.messages.fetch({ limit: 100 })).filter(
        (msg) =>
            msg.author.id === bot.user.id &&
            msg.embeds[0]?.description === embedMessage.description
    )
    await channel.bulkDelete(messages)

    await message.delete()
}

module.exports = {
    createThread,
    deleteMessage,
    sendEmbededMessage,
}
