const { handleSendMessage } = require('./regexp')
const { HOUR_IN_MS } = require('./constants')

module.exports = (bot) => {
    const cache = Object.create(null)

    setInterval(() => {
        for (const [key, value] of Object.entries(cache)) {
            const { createdAt } = value
            const currentTimeInMS = Date.now()
            const createdAtInMs = new Date(createdAt).getTime()

            if (currentTimeInMS - createdAtInMs > HOUR_IN_MS) {
                delete cache[key]
            }
        }
    }, 1000)

    bot.on('messageCreate', (message) => handleSendMessage(message, cache))
    bot.on('messageUpdate', (oldMessage, newMessage) =>
        handleSendMessage(newMessage, cache)
    )
}
