const { muteAuthorByBadWord } = require('./helpers')
const { regexp, AMOUNT_OF_WORDS_FOR_MUTE } = require('./constants')

module.exports = {
    handleSendMessage(message, cache) {
        try {
            const { content, author } = message
            const authorId = author.id
            const authorSignatureFromCache = cache[authorId]
            const incrementedBadWordsAmount =
                authorSignatureFromCache?.badWordsAmount
                    ? authorSignatureFromCache.badWordsAmount + 1
                    : 1
            if (regexp.test(content.toLowerCase()) && !content.includes("web")) {
                cache[authorId] = {
                    ...cache[authorId],
                    badWordsAmount: incrementedBadWordsAmount,
                }
                if (!cache[authorId].createdAt)
                    cache[authorId].createdAt = new Date()
            }

            if (cache[authorId]?.badWordsAmount === AMOUNT_OF_WORDS_FOR_MUTE) {
                muteAuthorByBadWord(message, author)
                delete cache[authorId]
            }
        } catch (e) {
            console.log(e)
        }
    },
}
