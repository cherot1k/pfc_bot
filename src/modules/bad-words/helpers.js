const { HOUR_IN_MS } = require('./constants')

const writeMessageToAuthor = (author, message) => {
    author.send(message)
}

const muteAuthorByBadWord = (message, author) => {
    try {
        const member = message.guild.members.cache.get(author.id)
        member.timeout(HOUR_IN_MS, 'Bad words use').catch((e) => console.log(e))
        writeMessageToAuthor(
            author,
            'Вы получили мут на сервере Passion for coding на один час, причина: использование ненормативной лексики'
        )
    } catch (e) {
        console.log(e)
    }
}

module.exports = {
    muteAuthorByBadWord,
    writeMessageToAuthor,
}
