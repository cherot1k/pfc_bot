const fs = require('fs')
const Path = require('path')

/**
 * @param path {String}
 * @param [result]
 * @description this function recursively applies to require for each file
 * @return {Array<{
 *     fileName: String,
 *     parentDirectory: {
 *         name: String,
 *         path: String
 *     },
 *     getData: () => Object
 * }>}
 * **/
function importDirectory(path, result = []) {
    const fileNames = fs.readdirSync(path)
    fileNames.forEach((fileName) => {
        const filePath = Path.resolve(path, fileName)
        const stat = fs.statSync(filePath)
        if (stat.isDirectory()) {
            importDirectory(filePath, result)
        } else {
            result.push({
                fileName,
                parentDirectory: {
                    name: path.split('\\').pop(),
                    path,
                },
                getData() {
                    return require(filePath)
                },
            })
        }
    })

    return result
}

module.exports = {
    importDirectory,
}
